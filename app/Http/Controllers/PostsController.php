<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Posts;
use App\Exceptions\{PostNotFoundException, DBConnection};
use Illuminate\Database\QueryException;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Posts::all()->reduce(function ($carry, $item) {
            $json = json_decode($item->details);
            $carry[] = [
                'id' => $item->id,
                'details' => $json,
                'images' => $item->images,
                'channel' => $item->channel,
                'created_at' => $item->created_at,
            ];
            return $carry;
        }, []);
        $posts=[];
        throw_if(empty($posts), PostNotFoundException::class);
        // dd($posts);
        return response()->json($posts);
    }


    public function create(Request $request)
    {   
        try {
            $this->validate($request, [
                'details' => 'required',
                'images' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
                'channel' => 'required',
            ]);
            $images = $request->file('images')->getClientOriginalName();
            $request->file('images')->move('upload', $images);
            $data = $request->all();
            // convert data to json
            $data['details'] = json_encode($data['details']);
            $data = Posts::create([
                'details' => $data['details'],
                'images' => $data['images'],
                'channel' => $data['channel'],
                'created_at' => Carbon::now()->format('d-m-Y h:i:s')
            ]);
            if ($data) {
                // Log::notice('Create Post Success', ['data' => $data]);
                return response()->json([
                    'status' => 'OK',
                    'code' => 200, 
                    'message' => 'Success', 
                    'created_at' => Carbon::now()->format('d-m-Y h:i:s'),
                ]);
            } 
        } catch (QueryException $exeption) {
            // dd($exeption->getCode());
            throw_if ($exeption->getCode() == 2002, DBConnection::class);
        }
    }
}
