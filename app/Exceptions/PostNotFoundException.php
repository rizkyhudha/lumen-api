<?php

namespace App\Exceptions;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class PostNotFoundException extends Exception
{
    public function report()
    {
        $code = 404;
        $message = "Post Not Found";
        $data = [
            'status' => 'Failed',
            'code' => $code,
            'message' => $message,
            'created_at' => Carbon::now()->format('d-m-Y h:i:s'),
        ];
        Log::error($message. " response:". json_encode($data), $data);
    }

    public function render()
    {
        return response()->json([
            'status' => 'Failed',
            'code' => 404,
            'message' => 'Post Not Found',
        ], 404);
    }
}
