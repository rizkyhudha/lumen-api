<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class DBConnection extends Exception
{
    public function report()
    {
        $code = 500;
        $message = "Database Connection Error";
        $data = [
            'status' => 'Failed',
            'code' => $code,
            'message' => $message,
            'created_at' => Carbon::now()->format('d-m-Y h:i:s'),
        ];
        Log::error($message. " response:". json_encode($data), $data);
    }

    public function render()
    {
        return response()->json([
            'status' => 'Failed',
            'code' => 500,
            'message' => 'Database Connection Error',
        ], 500);
    }
}
